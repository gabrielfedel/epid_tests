# Load ECMC
cd /home/iocuser/ecmctraining/startup/ecmcProject_PRL
< start.cmd

# Set paramaters
epicsEnvSet("P",    "$(ECMC_PREFIX)")
epicsEnvSet("R",    "1")
epicsEnvSet("SLAVE_NUM", 8) # probably change this to be loaded in ecmcPRL
epicsEnvSet("HW_TYPE_IN", "EL3202") # same as above
epicsEnvSet("INP",    "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s$(SLAVE_NUM)-$(HW_TYPE_IN)-AI")
epicsEnvSet("INPA",    "$(INP)1")
epicsEnvSet("INPB",    "$(INP)2")

epicsEnvSet("PID", "PID$(R)") #
epicsEnvSet("HW_TYPE_OUT", "EL2502") #
epicsEnvSet("SLAVE_NUM_OUT", "1") #
epicsEnvSet("OUT", "$(ECMC_PREFIX)ec$(ECMC_EC_MASTER_ID)-s$(SLAVE_NUM_OUT)-$(HW_TYPE_OUT)-BO1")
epicsEnvSet("LOW","0")
epicsEnvSet("HIGH","100")
epicsEnvSet("PREC","6")
epicsEnvSet("KP","6")
epicsEnvSet("KI","0.007194")
epicsEnvSet("KD","0.0")
epicsEnvSet("SCAN",".5 second")


cd /home/iocuser/epid_tests/prl

# ai-avg will generate average from 2 ai's
dbLoadRecords("ai-avg.db", "P=$(P), R=$(R), INPA=$(INPA), INPB=$(INPB)")

require std
require calc

# PID record
dbLoadRecords("/home/iocuser/e3-3.15.5/e3-std/std/stdApp/Db/pid_control.db", "P=$(P), PID=$(PID), INP=$(P)AI-AVG$(R), OUT=$(OUT), LOPR=$(LOW), HOPR=$(HIGH), DRVL=$(LOW), DRVH=$(HIGH), PREC=$(PREC), KP=$(KP), KI=$(KI), KD=$(KD),  SCAN=$(SCAN)")


afterInit("dbpf $(ECMC_PREFIX)PID1.FBON 1")

